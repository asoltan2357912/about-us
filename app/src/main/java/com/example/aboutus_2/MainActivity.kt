package com.example.aboutus_2

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.wq.android.lib.verticaltablayout.VerticalTabLayout


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val vTabLayout = findViewById(R.id.tab_layout_1) as VerticalTabLayout
//        vTabLayout.addTab(
//            vTabLayout.newTab().setText("God")
//        )
//        vTabLayout.addTab(
//            vTabLayout.newTab().setText("Saints")
//        )
//        vTabLayout.addTab(
//            vTabLayout.newTab().setText("Aacharya")
//        )
//        vTabLayout.addTab(
//            vTabLayout.newTab().setText("Scriptures")
//        )
//        vTabLayout.addTab(
//            vTabLayout.newTab().setText("Temples")
//        )
//        vTabLayout.addTab(
//            vTabLayout.newTab().setText("Devotees")
//        )
        val iv = findViewById(R.id.detail) as ImageView
        iv.setImageDrawable(getDrawable(R.drawable.login_background))
        buttonGodAction(findViewById(R.id.button_god))
    }

    @SuppressLint("ResourceType")
    fun buttonGodAction(v: View?) {
        val tv = findViewById(R.id.button_god) as TextView
        looseFocus(tv)
        tv.setTextColor(Color.parseColor(getString(R.color.colorPrimary)))

//        val iv = findViewById(R.id.imageView) as ImageView
//        iv.setImageResource(R.drawable.shringaar)
//        Glide.with(iv.context)
//            .load("https://img.youtube.com/vi/VzX_4AyzXd0/hqdefault.jpg")
//            .centerCrop()
//            .into(iv)

        val tt = findViewById(R.id.text_title) as TextView
        tt.text = getString(R.string.text_God_1)

        val tc = findViewById(R.id.text_content) as TextView
        tc.text = getString(R.string.text_content_God_1)
    }

    @SuppressLint("ResourceType")
    fun buttonSaintsAction(v: View?) {
        val tv = findViewById(R.id.button_saints) as TextView
        looseFocus(tv)
        tv.setTextColor(Color.parseColor(getString(R.color.colorPrimary)))

        val tt = findViewById(R.id.text_title) as TextView
        tt.text = getString(R.string.text_Saints_1)

        val tc = findViewById(R.id.text_content) as TextView
        tc.text = getString(R.string.text_content_Saints_1)
    }

    @SuppressLint("ResourceType")
    fun buttonAacharyaAction(v: View?) {
        val tv = findViewById(R.id.button_aacharya) as TextView
        looseFocus(tv)
        tv.setTextColor(Color.parseColor(getString(R.color.colorPrimary)))

        val tt = findViewById(R.id.text_title) as TextView
        tt.text = getString(R.string.text_Aacharya_1)

        val tc = findViewById(R.id.text_content) as TextView
        tc.text = getString(R.string.text_content_Aacharya_1)
    }

    @SuppressLint("ResourceType")
    fun buttonScripturesAction(v: View?) {
        val tv = findViewById(R.id.button_scriptures) as TextView
        looseFocus(tv)
        tv.setTextColor(Color.parseColor(getString(R.color.colorPrimary)))

        val tt = findViewById(R.id.text_title) as TextView
        tt.text = getString(R.string.text_Scriptures_1)

        val tc = findViewById(R.id.text_content) as TextView
        tc.text = getString(R.string.text_content_Scriptures_1)
    }

    @SuppressLint("ResourceType")
    fun buttonTemplesAction(v: View?) {
        val tv = findViewById(R.id.button_temples) as TextView
        looseFocus(tv)
        tv.setTextColor(Color.parseColor(getString(R.color.colorPrimary)))

        val tt = findViewById(R.id.text_title) as TextView
        tt.text = getString(R.string.text_Temples_1)

        val tc = findViewById(R.id.text_content) as TextView
        tc.text = getString(R.string.text_content_Temples_1)
    }

    @SuppressLint("ResourceType")
    fun buttonDevoteesAction(v: View?) {
        val tv = findViewById(R.id.button_devotees) as TextView
        looseFocus(tv)
        tv.setTextColor(Color.parseColor(getString(R.color.colorPrimary)))

        val tt = findViewById(R.id.text_title) as TextView
        tt.text = getString(R.string.text_Devotees_1)

        val tc = findViewById(R.id.text_content) as TextView
        tc.text = getString(R.string.text_content_Devotees_1)
    }

    @SuppressLint("ResourceAsColor")
    fun looseFocus(v:View){
        var tv = findViewById(R.id.button_god) as TextView
        if(!v.equals(tv))
            tv.setTextColor(R.color.colorShadow)
        tv = findViewById(R.id.button_saints) as TextView
        if(!v.equals(tv))
            tv.setTextColor(R.color.colorShadow)
        tv = findViewById(R.id.button_aacharya) as TextView
        if(!v.equals(tv))
            tv.setTextColor(R.color.colorShadow)
        tv = findViewById(R.id.button_scriptures) as TextView
        if(!v.equals(tv))
            tv.setTextColor(R.color.colorShadow)
        tv = findViewById(R.id.button_temples) as TextView
        if(!v.equals(tv))
            tv.setTextColor(R.color.colorShadow)
        tv = findViewById(R.id.button_devotees) as TextView
        if(!v.equals(tv))
            tv.setTextColor(R.color.colorShadow)
    }
}