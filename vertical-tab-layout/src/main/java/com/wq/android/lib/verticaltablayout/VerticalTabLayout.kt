package com.wq.android.lib.verticaltablayout

import android.animation.ValueAnimator
import android.annotation.TargetApi
import android.content.Context
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView

/**
 * Created by qwang on 2016/8/10.
 */
class VerticalTabLayout : ScrollView, View.OnClickListener {
    private var mTabStrip: TabStrip? = null
    private var mIndicatorColor = Color.GRAY
    private lateinit var mContext: Context
    private var mIndicatorWidth = Int.MAX_VALUE
    private var mIndicatorLeft = 0
    private var mIndicatorY = 0f
    private var mIndicatorRight = 0
    private var mIndicatorCorners = 0
    private var mDividerHeight = 0
    private var mDividerColor = Color.GRAY
    private var mIndicatorGravity = Gravity.FILL
    private var mTabHeight = INVALID_NUM
    private var mDividerPadding = 0
    private var mArrowColor = Color.TRANSPARENT
    private var mArrowGravity = Gravity.RIGHT
    private var mTabTextColor = Color.DKGRAY
    private var mTabSelectedTextColor = Color.DKGRAY
    private var mTabTextAppearance = INVALID_NUM
    private var mTabTextSize = INVALID_NUM
    private var mTabPadding = INVALID_NUM
    private var mTabPaddingLeft = 0
    private var mTabPaddingRight = 0
    private var mTabPaddingTop = 0
    private var mTabPaddingBottom = 0
    private var mTabGravity = Gravity.CENTER_VERTICAL
    private var mOnTabSelectedListener: OnTabSelectedListener? =
        null
    private var mArrowSize = 0
    private var mArrowType = ARROW_TYPE_INNER
    private var mTabDrawablePadding = 0

    interface OnTabSelectedListener {
        fun onTabSelected(
            tab: Tab?,
            position: Int
        )

        fun onTabReleased(
            tab: Tab?,
            position: Int
        )
    }

    class OnTabSelectedAdapter :
        OnTabSelectedListener {
        override fun onTabSelected(
            tab: Tab?,
            position: Int
        ) {
        }

        override fun onTabReleased(
            tab: Tab?,
            position: Int
        ) {
        }
    }

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    private fun init(
        context: Context,
        attrs: AttributeSet?
    ) {
        mContext = context
        isVerticalScrollBarEnabled = false
        isHorizontalScrollBarEnabled = false
        mTabStrip = TabStrip(mContext)
        super.addView(
            mTabStrip,
            0,
            LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
            )
        )
        isFillViewport = true
        val typedArray = mContext!!.obtainStyledAttributes(
            attrs,
            R.styleable.VerticalTabLayout
        )
        mIndicatorColor = typedArray.getColor(
            R.styleable.VerticalTabLayout_tabIndicatorColor,
            mIndicatorColor
        )
        mArrowColor = typedArray.getColor(
            R.styleable.VerticalTabLayout_tabArrowColor,
            mArrowColor
        )
        mArrowGravity = typedArray.getInteger(
            R.styleable.VerticalTabLayout_tabArrowGravity,
            mArrowGravity
        )
        mIndicatorWidth = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabIndicatorWidth,
            dpToPx(5).toFloat()
        ).toInt()
        mIndicatorCorners = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabIndicatorCorners,
            mDividerHeight.toFloat()
        ).toInt()
        mDividerHeight = typedArray.getDimension(
            R.styleable.VerticalTabLayout_dividerHeight,
            mDividerHeight.toFloat()
        ).toInt()
        mDividerColor = typedArray.getColor(
            R.styleable.VerticalTabLayout_dividerColor,
            Color.TRANSPARENT
        )
        mDividerPadding = typedArray.getDimension(
            R.styleable.VerticalTabLayout_dividerPadding,
            mDividerPadding.toFloat()
        ).toInt()
        mIndicatorGravity = typedArray.getInteger(
            R.styleable.VerticalTabLayout_tabIndicatorGravityNew,
            mIndicatorGravity
        )
        mTabHeight = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabHeight,
            LayoutParams.MATCH_PARENT.toFloat()
        ).toInt()
        mTabTextColor = typedArray.getColor(
            R.styleable.VerticalTabLayout_tabTextColor,
            mTabTextColor
        )
        mTabSelectedTextColor = typedArray.getColor(
            R.styleable.VerticalTabLayout_tabSelectedTextColor,
            mTabSelectedTextColor
        )
        mTabTextAppearance = typedArray.getResourceId(
            R.styleable.VerticalTabLayout_tabTextAppearance,
            mTabTextAppearance
        )
        mTabTextSize = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabTextSize,
            mTabTextSize.toFloat()
        ).toInt()
        mTabPadding = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabPadding,
            mTabPadding.toFloat()
        ).toInt()
        mTabPaddingLeft = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabPaddingLeft,
            mTabPaddingLeft.toFloat()
        ).toInt()
        mTabPaddingRight = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabPaddingRight,
            mTabPaddingRight.toFloat()
        ).toInt()
        mTabPaddingTop = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabPaddingTop,
            mTabPaddingTop.toFloat()
        ).toInt()
        mTabPaddingBottom = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabPaddingBottom,
            mTabPaddingBottom.toFloat()
        ).toInt()
        mTabGravity = typedArray.getInteger(
            R.styleable.VerticalTabLayout_tabViewGravity,
            mTabGravity
        )
        mArrowType = typedArray.getInteger(
            R.styleable.VerticalTabLayout_tabArrowType,
            mArrowType
        )
        mTabDrawablePadding = typedArray.getDimension(
            R.styleable.VerticalTabLayout_tabIconPadding,
            mTabDrawablePadding.toFloat()
        ).toInt()
        typedArray.recycle()
        initLayout()
    }

    private fun initLayout() {
        post {
            initBackgroundForArrow()
            initDivider()
            initIndicatorGravity()
        }
    }

    fun setOnTabSelectedListener(listener: OnTabSelectedListener?) {
        mOnTabSelectedListener = listener
    }

    fun addTab(tab: Tab?) {
        val tabView =
            TabView(mContext, tab!!)
        mTabStrip!!.addView(tabView)
        tabView.setOnClickListener(this)
    }

    /**
     * Create and return a new [Tab]. You need to manually add this using
     * [.addTab] or a related method.
     *
     * @return A new Tab
     * @see .addTab
     */
    fun newTab(): Tab {
        return Tab()
    }

    override fun onClick(view: View) {
        setSelectedTab(view)
    }

    fun setSelectedTab(position: Int) {
        setSelectedTab(mTabStrip!!.getChildAt(position))
    }

    private var mSelectedTab: View? = null
    private fun setSelectedTab(view: View?) {
        if (view == null) {
            return
        }
        if (mSelectedTab != null) {
            mSelectedTab!!.isSelected = false
        }
        view.isSelected = true
        mSelectedTab = view
        scrollTab(mSelectedTab!!)
        mTabStrip!!.animateIndicator(view)
        mTabStrip!!.invalidate()
    }

    private fun scrollTab(tabView: View) {
        tabView.post {
            val tabTop = tabView.top + tabView.height / 2 - scrollY
            val target = height / 2
            if (tabTop > target) {
                smoothScrollBy(0, tabTop - target)
            } else if (tabTop < target) {
                smoothScrollBy(0, tabTop - target)
            }
        }
    }

    fun setTabHeight(px: Int) {
        mTabHeight = px
        mTabStrip!!.postInvalidate()
    }

    fun setArrowColor(color: Int) {
        mArrowColor = color
    }

    fun removeAllTabs() {
        mTabStrip!!.removeAllViews()
        mIndicatorY = 0f
        postInvalidate()
    }

    fun initBackgroundForArrow() {
        if (mArrowColor != Color.TRANSPARENT) {
            mArrowSize = mTabHeight / 6
            if (mArrowType == ARROW_TYPE_OUTER) {
                var background = background
                background =
                    background ?: ColorDrawable()
                val d =
                    LayerDrawable(
                        arrayOf(background)
                    )
                if (mArrowGravity == Gravity.RIGHT) {
                    d.setLayerInset(0, 0, 0, mArrowSize, 0)
                } else if (mArrowGravity == Gravity.LEFT) {
                    d.setLayerInset(0, mArrowSize, 0, 0, 0)
                }
                setBackground(d)
            }
        }
    }

    fun setDivider(height: Int, color: Int, padding: Int) {
        mDividerColor = color
        mDividerHeight = height
        mDividerPadding = padding
    }

    private fun initDivider() {
        if (mDividerHeight > 0) {
            val drawable =
                GradientDrawable()
            drawable.setColor(mDividerColor)
            drawable.setSize(1, mDividerHeight)
            mTabStrip!!.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
            if (mArrowType == ARROW_TYPE_OUTER && mArrowSize > 0 && mArrowColor != Color.TRANSPARENT) {
                val layerDrawable =
                    LayerDrawable(
                        arrayOf<Drawable>(drawable)
                    )
                if (mArrowGravity == Gravity.LEFT) {
                    layerDrawable.setLayerInset(0, mArrowSize, 0, 0, 0)
                } else {
                    layerDrawable.setLayerInset(0, 0, 0, mArrowSize, 0)
                }
                mTabStrip!!.dividerDrawable = layerDrawable
            } else {
                mTabStrip!!.dividerDrawable = drawable
            }
            mTabStrip!!.dividerPadding = mDividerPadding
            mTabStrip!!.postInvalidate()
        }
    }

    private fun initIndicatorGravity() {
        if (mArrowType == ARROW_TYPE_OUTER && mArrowColor != Color.TRANSPARENT) {
            calcIndicatorSizeByArrowOuter()
        } else {
            calcIndicatorSizeByArrowInner()
        }
    }

    private fun calcIndicatorSizeByArrowOuter() {
        val width = mTabStrip!!.width
        if (mIndicatorGravity == Gravity.FILL || mIndicatorWidth >= width) {
            mIndicatorLeft = if (0 + mArrowGravity == Gravity.LEFT) mArrowSize else 0
            mIndicatorRight =
                width - if (mArrowGravity == Gravity.RIGHT) mArrowSize else 0
        } else if (mIndicatorGravity == Gravity.LEFT) {
            mIndicatorLeft = 0 + mArrowSize
            mIndicatorRight = mIndicatorWidth
        } else {
            mIndicatorLeft = width - mIndicatorWidth - mArrowSize
            mIndicatorRight = width - mArrowSize
        }
        mTabStrip!!.invalidate()
    }

    private fun calcIndicatorSizeByArrowInner() {
        val width = mTabStrip!!.width
        if (mIndicatorGravity == Gravity.FILL || mIndicatorWidth >= width) {
            mIndicatorLeft = 0
            mIndicatorRight = width
        } else if (mIndicatorGravity == Gravity.LEFT) {
            mIndicatorLeft = 0
            mIndicatorRight = mIndicatorWidth
        } else {
            mIndicatorLeft = width - mIndicatorWidth
            mIndicatorRight = width
        }
        mTabStrip!!.invalidate()
    }

    override fun addView(child: View) {
        addViewInternal(child)
    }

    override fun addView(child: View, index: Int) {
        addViewInternal(child)
    }

    override fun addView(
        child: View,
        index: Int,
        params: ViewGroup.LayoutParams
    ) {
        addViewInternal(child)
    }

    override fun addView(
        child: View,
        params: ViewGroup.LayoutParams
    ) {
        addViewInternal(child)
    }

    private fun addViewInternal(child: View) {
        if (child is TabItem) {
            addTabFromItemView(child)
        } else {
            throw IllegalArgumentException("Only TabItem instances can be added to TabLayout")
        }
    }

    private fun addTabFromItemView(item: TabItem) {
        val tab = newTab()
        tab.setText(item.text)
        tab.setIcon(item.icon)
        tab.setIconGravity(item.iconGravity)
        tab.setIconSize(item.iconWidth, item.iconHeight)
        tab.setContentDescription(
            if (item.contentDescription != null) item.contentDescription
                .toString() else null
        )
        addTab(tab)
    }

    override fun removeView(view: View) {}
    override fun removeAllViews() {}
    override fun removeViewAt(index: Int) {}
    private inner class TabStrip internal constructor(context: Context?) :
        LinearLayout(context) {
        private val mPaint: Paint
        private val mRect = RectF()
        private val mArrowPath = Path()
        fun animateIndicator(view: View) {
            val moveTo = view.top.toFloat()
            if (mIndicatorY == moveTo) {
                return
            }
            if (mAnimator != null && mAnimator!!.isRunning) {
                mAnimator!!.cancel()
            }
            mAnimator =
                ValueAnimator.ofInt(mIndicatorY.toInt(), moveTo.toInt())
            mAnimator!!.addUpdateListener(ValueAnimator.AnimatorUpdateListener { valueAnimator ->
                val value = valueAnimator.animatedValue.toString().toFloat()
                mIndicatorY = value
                invalidate()
            })
            mAnimator!!.setDuration(200).start()
        }

        override fun addView(child: View) {
            super.addView(child, createLayoutParamsForTabs())
        }

        private fun createLayoutParamsForTabs(): LayoutParams {
            val lp =
                LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT
                )
            lp.height =
                if (mTabHeight <= 0) FrameLayout.LayoutParams.MATCH_PARENT else mTabHeight
            return lp
        }

        public override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            if (childCount <= 0 || mSelectedTab == null) {
                return
            }
            val width = width
            mPaint.reset()
            //drawShadow(canvas, width);
            drawIndicator(canvas)
            drawArrow(canvas, width)
        }

        private fun drawShadow(canvas: Canvas, width: Int) {
            if (mIndicatorGravity != Gravity.FILL && mIndicatorWidth < width) {
                mPaint.color = SHADOW_COLOR
                mRect[0f, mIndicatorY, width.toFloat()] = mIndicatorY + mTabHeight
                canvas.drawRect(mRect, mPaint)
            }
        }

        private fun drawIndicator(canvas: Canvas) {
            mRect[mIndicatorLeft.toFloat(), mIndicatorY, mIndicatorRight.toFloat()] =
                mIndicatorY + mTabHeight
            mPaint.color = mIndicatorColor
            if (mIndicatorCorners > 0) {
                canvas.drawRoundRect(
                    mRect,
                    mIndicatorCorners.toFloat(),
                    mIndicatorCorners.toFloat(),
                    mPaint
                )
            } else {
                canvas.drawRect(mRect, mPaint)
            }
        }

        private fun drawArrow(canvas: Canvas, width: Int) {
            if (mArrowColor == Color.TRANSPARENT || mArrowSize <= 0) {
                return
            }
            if (mArrowType == ARROW_TYPE_OUTER) {
                drawArrowOuter(canvas, width)
            } else {
                drawArrowInner(canvas, width)
            }
        }

        private fun drawArrowInner(canvas: Canvas, width: Int) {
            mArrowPath.reset()
            if (mArrowGravity == Gravity.RIGHT) {
                mArrowPath.moveTo(width - mArrowSize.toFloat(), mIndicatorY + mTabHeight / 2)
                mArrowPath.lineTo(
                    width + mArrowSize.toFloat(),
                    mIndicatorY + mTabHeight / 2 - mArrowSize * 2
                )
                mArrowPath.lineTo(
                    width + mArrowSize.toFloat(),
                    mIndicatorY + mTabHeight / 2 + mArrowSize * 2
                )
                mArrowPath.close()
            } else {
                mArrowPath.moveTo(mArrowSize.toFloat(), mIndicatorY + mTabHeight / 2)
                mArrowPath.lineTo(0f, mIndicatorY + mTabHeight / 2 - mArrowSize)
                mArrowPath.lineTo(0f, mIndicatorY + mTabHeight / 2 + mArrowSize)
                mArrowPath.close()
            }
            mPaint.color = mArrowColor
            canvas.drawPath(mArrowPath, mPaint)
        }

        private fun drawArrowOuter(canvas: Canvas, width: Int) {
            mArrowPath.reset()
            if (mArrowGravity == Gravity.RIGHT) {
                mArrowPath.moveTo(width.toFloat(), mIndicatorY + mTabHeight / 2)
                mArrowPath.lineTo(
                    width - mArrowSize.toFloat(),
                    mIndicatorY + mTabHeight / 2 - mArrowSize
                )
                mArrowPath.lineTo(
                    width - mArrowSize.toFloat(),
                    mIndicatorY + mTabHeight / 2 + mArrowSize
                )
                mArrowPath.close()
            } else {
                mArrowPath.moveTo(0f, mIndicatorY + mTabHeight / 2)
                mArrowPath.lineTo(mArrowSize.toFloat(), mIndicatorY + mTabHeight / 2 - mArrowSize)
                mArrowPath.lineTo(mArrowSize.toFloat(), mIndicatorY + mTabHeight / 2 + mArrowSize)
                mArrowPath.close()
            }
            mPaint.color = mArrowColor
            canvas.drawPath(mArrowPath, mPaint)
        }

        init {
            setWillNotDraw(false)
            orientation = VERTICAL
            mPaint = Paint()
            //mPaint.setAntiAlias(true);
        }
    }

    private fun dpToPx(dps: Int): Int {
        return Math.round(resources.displayMetrics.density * dps)
    }

    class Tab {
        var drawable: Drawable? = null
            private set
        var drawableRes = INVALID_NUM
            private set
        var drawableGravity = Gravity.LEFT
            private set
        var text: String? = null
            private set
        var contentDescription: String? = ""
            private set
        var drawableWidth = 0
            private set
        var drawableHeight = 0
            private set
        var customView: View? = null
            private set
        var tag: Any? = null
            private set

        fun setContentDescription(contentDescription: String?): Tab {
            this.contentDescription = contentDescription
            return this
        }

        fun setIcon(drawable: Drawable?): Tab {
            this.drawable = drawable
            return this
        }

        fun setIcon(drawableRes: Int): Tab {
            this.drawableRes = drawableRes
            return this
        }

        fun setIconGravity(drawableGravity: Int): Tab {
            this.drawableGravity = drawableGravity
            return this
        }

        fun setIconSize(
            drawableWidth: Int,
            drawableHeight: Int
        ): Tab {
            this.drawableWidth = drawableWidth
            this.drawableHeight = drawableHeight
            return this
        }

        fun setText(text: String?): Tab {
            this.text = text
            return this
        }

        fun setCustomView(mCustomView: View?): Tab {
            customView = mCustomView
            return this
        }

        fun setCustomView(
            context: Context?,
            layoutId: Int
        ): Tab {
            customView = LayoutInflater.from(context).inflate(layoutId, null)
            return this
        }

        fun setTag(mTag: Any?): Tab {
            tag = mTag
            return this
        }
    }

    private inner class TabView(
        context: Context,
        private val mTab: Tab
    ) : FrameLayout(context) {
        private val mTabItem: TextView?
        private fun setPaddings() {
            if (mTabPadding > 0) {
                mTabItem!!.setPadding(mTabPadding, mTabPadding, mTabPadding, mTabPadding)
            } else if (mTabPaddingLeft > 0 || mTabPaddingTop > 0 || mTabPaddingRight > 0 || mTabPaddingBottom > 0) {
                mTabItem!!.setPadding(
                    mTabPaddingLeft,
                    mTabPaddingTop,
                    mTabPaddingRight,
                    mTabPaddingBottom
                )
            }
        }

        private fun createLayoutParams(): LayoutParams {
            val params =
                LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            params.gravity = mTabGravity or Gravity.CENTER_VERTICAL
            return params
        }

        private fun setDrawable() {
            var d = mTab.drawable
            if (d == null && mTab.drawableRes != INVALID_NUM) {
                d = resources.getDrawable(mTab.drawableRes)
            }
            if (d == null) {
                return
            }
            d.isFilterBitmap = true
            if (mTab.drawableHeight > 0 && mTab.drawableWidth > 0) {
                d.setBounds(0, 0, mTab.drawableWidth, mTab.drawableHeight)
            } else {
                val width = d.intrinsicWidth.toFloat()
                val height = d.intrinsicHeight.toFloat()
                val rate = width / height
                var bottom = mTabHeight / 2.toFloat()
                if (bottom > height) bottom = height
                val right = bottom * rate
                d.setBounds(0, 0, right.toInt(), bottom.toInt())
            }
            when (mTab.drawableGravity) {
                Gravity.LEFT -> mTabItem!!.setCompoundDrawables(d, null, null, null)
                Gravity.RIGHT -> mTabItem!!.setCompoundDrawables(null, null, d, null)
                Gravity.TOP -> mTabItem!!.setCompoundDrawables(null, d, null, null)
                Gravity.BOTTOM -> mTabItem!!.setCompoundDrawables(null, null, null, d)
            }
            mTabItem!!.compoundDrawablePadding = mTabDrawablePadding
        }

        override fun setSelected(selected: Boolean) {
            super.setSelected(selected)
            mTabItem?.setTextColor(if (selected) mTabSelectedTextColor else mTabTextColor)
            if (mOnTabSelectedListener != null) {
                if (selected) {
                    mOnTabSelectedListener!!.onTabSelected(mTab, mTabStrip!!.indexOfChild(this))
                } else {
                    mOnTabSelectedListener!!.onTabReleased(mTab, mTabStrip!!.indexOfChild(this))
                }
            }
        }

        init {
            contentDescription = mTab.contentDescription
            if (mTab.customView != null) {
                addView(mTab.customView)
//                return
            }
            mTabItem = TextView(mContext)
            mTabItem.text = mTab.text
            mTabItem.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTabTextSize.toFloat())
            mTabItem.setTextColor(mTabTextColor)
            mTabItem.gravity = Gravity.CENTER_VERTICAL
            setPaddings()
            setDrawable()
            addView(mTabItem, createLayoutParams())
        }
    }

    companion object {
        private const val INVALID_NUM = -1
        private const val SHADOW_COLOR = 0x22000000
        private const val ARROW_TYPE_INNER = 1
        private const val ARROW_TYPE_OUTER = 2
        private var mAnimator: ValueAnimator? = null
    }
}