package com.wq.android.lib.verticaltablayout

import android.annotation.TargetApi
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.view.Gravity
import android.view.View

/**
 * Created by qwang on 2016/8/12.
 */
class TabItem : View {
    var icon: Drawable? = null
    var text: String? = null
    var iconGravity = Gravity.LEFT
    var iconWidth = 0
    var iconHeight = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    private fun init(
        context: Context,
        attrs: AttributeSet?
    ) {
        if (attrs == null) {
            return
        }
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.TabItem
        )
        icon =
            typedArray.getDrawable(R.styleable.TabItem_icon)
        text = typedArray.getString(R.styleable.TabItem_text)
        iconGravity = typedArray.getInt(
            R.styleable.TabItem_iconGravityNew,
            iconGravity
        )
        iconWidth = typedArray.getDimension(
            R.styleable.TabItem_iconWidth,
            0f
        ).toInt()
        iconHeight = typedArray.getDimension(
            R.styleable.TabItem_iconHeight,
            0f
        ).toInt()
        typedArray.recycle()
    }

}